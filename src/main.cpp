/*
Arduino-MAX30102 oximetry / heart rate integrated sensor library
Copyright (C) 2016  OXullo Intersecans <x@brainrapers.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <Arduino.h>
#include <Wire.h>
#include "MAX30102_PulseOximeter.h"
#include "painlessMesh.h"
#include <U8g2lib.h>


// PulseOximeter is the higher level interface to the sensor
// it offers:
//  * beat detection reporting
//  * heart rate calculation
//  * SpO2 (oxidation level) calculation
PulseOximeter pox;


// mesh network
#define MESH_PREFIX "whateverYouLike"
#define MESH_PASSWORD "somethingSneaky"
#define MESH_PORT 5555
#define ROLE "purple"
SimpleList<uint32_t> nodes;          // number of nodes connected 0 = no connection
uint32_t nsent = 0;                  // total messages sent, makes the client aware of missing messages
char CONNECTED[32] = "disconnected"; // this node is disconnected at startup
char NODE_ID[16];
char CALLBACK[32] = "Name unknown"; // stores the extracted patient name
char TOTALMSG[6] = "0";
Scheduler userScheduler; // send spo2/hr every 10 sec
painlessMesh mesh;

/*MAX30105 buffer for heart rate measurements*/
const uint8_t RATE_SIZE = 4; //Increase this for more averaging. 4 is good.
uint8_t rates[RATE_SIZE];    //Array of heart rates
uint8_t rateSpot = 0;
int beatAvg;
byte goToSleep = 0;  // counter to go to sleep when reached 6 (1minute)

U8G2_SSD1306_128X64_NONAME_1_SW_I2C u8g2(U8G2_R0, /* clock=*/15, /* data=*/4, /* reset=*/16);

void draw(){ 
    u8g2.firstPage();
    do {
        u8g2.drawFrame(0, 0, u8g2.getDisplayWidth(), u8g2.getDisplayHeight());
        u8g2.drawStr(3, 15, "HR:");
        u8g2.drawStr(40, 15, u8x8_u16toa(beatAvg, 2));
        u8g2.drawStr(3, 30, "SPO2:");
        u8g2.drawStr(40, 30, u8x8_u16toa(pox.getSpO2(), 2));
        u8g2.drawStr(3, 45, CONNECTED);
        u8g2.drawStr(80,45,u8x8_u16toa(nsent, 3));
        u8g2.drawStr(3, 60, CALLBACK);
        //u8g2.drawXBM
    } while (u8g2.nextPage());
}

void sendMessage()
{
    Serial.print("Heart rate:");
    Serial.print(beatAvg);
    Serial.print("bpm / SpO2:");
    Serial.print(pox.getSpO2());
    Serial.println("%");
    // send only when connected
    if (nodes.size() > 0)
    {
        DynamicJsonDocument jsonBuffer(1024);
        JsonObject msg = jsonBuffer.to<JsonObject>();
        //sprintf(msg["hr"], "%d", n_heart_rate);
        //sprintf(msg["spo2"], "%.0f", n_spo2);
        msg["nodeId"] = mesh.getNodeId();
        msg["role"] = ROLE;
        msg["hr"] = beatAvg;
        msg["spo2"] = pox.getSpO2();
        msg["msgno"] = nsent;
        msg["name"] = CALLBACK;
        String str;
        serializeJson(msg, str);
        mesh.sendBroadcast(str);
        Serial.printf("Tx-> %s\n", str.c_str()); // and to the serial console
        nsent++;                                 // increase total messages counter;
    }
    else
    {
        Serial.printf("NodeId %u is not connected. Not sending message\n", mesh.getNodeId());
    }
}

Task taskSendMessage(TASK_SECOND * 10, TASK_FOREVER, &sendMessage);
// Empfangen von Nachrichten
void receivedCallback(uint32_t from, String &msg)
{
    Serial.printf("Rx from %u <- %s\n", from, msg.c_str());

    DynamicJsonDocument doc(1024);

    // Incoming message must be a JSON String
    // String input =  "{\"id\":1351824120,\"name\":\"Hans Meier\"}";
    deserializeJson(doc, msg);
    JsonObject obj = doc.as<JsonObject>();

    // read the node id from json and if it matches read the name
    uint32_t id = obj[String("id")];
    if (id == mesh.getNodeId())
    {
        // You can get a String from a JsonObject or JsonArray:
        String name = obj["name"];

        sprintf(CALLBACK, "%s", name.c_str());
        Serial.printf(CALLBACK);

        draw();
    }
}

void newConnectionCallback(uint32_t nodeId)
{
    Serial.printf("--> Start: New Connection, nodeId = %u\n", nodeId);
    Serial.printf("--> Start: New Connection, %s\n", mesh.subConnectionJson(true).c_str());
}

void changedConnectionCallback()
{
    Serial.printf("Changed connections\n");

    nodes = mesh.getNodeList();
    Serial.printf("Num nodes: %d\n", nodes.size());
    Serial.printf("Connection list:");
    SimpleList<uint32_t>::iterator node = nodes.begin();
    while (node != nodes.end())
    {
        Serial.printf(" %u", *node);
        node++;
    }
    Serial.println();
    //calc_delay = true;
    if (nodes.size() > 0)
    {
        sprintf(CONNECTED, "con %d Nodes", nodes.size());
    }
    else
    {
        sprintf(CONNECTED, "disconnected");
    }
    draw();
}

void nodeTimeAdjustedCallback(int32_t offset)
{
    Serial.printf("Adjusted time %u Offset = %d\n", mesh.getNodeTime(), offset);
}

void onNodeDelayReceived(uint32_t nodeId, int32_t delay)
{
    Serial.printf("Delay from node:%u delay = %d\n", nodeId, delay);
}

// Max30102 Callback (registered below) fired when a pulse is detected
void onBeatDetected()
{
    Serial.println("Beat!");
    rates[rateSpot++] = (byte)pox.getHeartRate(); //Store this reading in the array
    rateSpot %= RATE_SIZE;                    //Wrap variable

    //Take average of readings and store in beatAvg (accessed in draw())
    beatAvg = 0;
    for (byte x = 0; x < RATE_SIZE; x++)
        beatAvg += rates[x];
    beatAvg /= RATE_SIZE;
    
    // measurements of beat < 30 are discarded and the counter for goToSleep increased
    if (beatAvg > 30){
        goToSleep = 0;
    } else{
       // goToSleep ++;
        beatAvg = 0;
       // Serial.printf("Go to sleep in %d sec", 60 - goToSleep * 10);
    } 
    draw();
}


void enterDeepsleep()
{
    u8g2.clearDisplay();
    u8g2.drawStr(3, 30, "Going to sleep");
    u8g2.drawStr(3,45,"Press again to wake up");
    Serial.println("Go to Sleep");
    pox.shutdown();
    mesh.stop();
    delay(3000);
    u8g2.sleepOn();
    u8g2.setPowerSave(1);
    esp_sleep_enable_ext1_wakeup(GPIO_SEL_38, ESP_EXT1_WAKEUP_ALL_LOW);
    esp_deep_sleep_start();
}

void setup()
{
    Serial.begin(115200);
    while (!Serial)
    {
        ;
    }
    delay(1000);
    Serial.print("Initializing pulse oximeter..");

    // Initialize the PulseOximeter instance
    // Failures are generally due to an improper I2C wiring, missing power supply
    // or wrong target chip
    if (!pox.begin())
    {
        Serial.println("FAILED");
        for (;;)
            ;
    }
    else
    {
        Serial.println("SUCCESS");
    }

    // The default current for the IR LED is 50mA and it could be changed
    //   by uncommenting the following line. Check MAX30100_Registers.h for all the
    //   available options.
    pox.setIRLedCurrent(MAX30102_LED_CURR_24MA);

    // Register a callback for the beat detection
    pox.setOnBeatDetectedCallback(onBeatDetected);

    mesh.setDebugMsgTypes(ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | MSG_TYPES | REMOTE); // all types on except GENERAL
    //mesh.setDebugMsgTypes(ERROR | STARTUP); // set before init() so that you can see startup messages

    mesh.init(MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT, WIFI_AP_STA, 6); // , WIFI_AP_STA, 6
    //mesh.init(MESH_PREFIX, MESH_PASSWORD, MESH_PORT);
    mesh.onReceive(&receivedCallback);
    mesh.onNewConnection(&newConnectionCallback);
    mesh.onChangedConnections(&changedConnectionCallback);
    mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
    // if you want your node to accept OTA firmware, simply include this line
    // with whatever role you want your hardware to be. For instance, aMessage
    mesh.initOTAReceive(ROLE);
    // This node and all other nodes should ideally know the mesh contains a root, so call this on all nodes
    mesh.setContainsRoot(true);
    Serial.printf("My Nodeid: %u\n", mesh.getNodeId());
    sprintf(NODE_ID, "Id:%d", mesh.getNodeId());

    userScheduler.addTask(taskSendMessage);
    taskSendMessage.enable();
 
    if (!u8g2.begin()) {
        Serial.println("OLED FAILED");
        for (;;)
            ;
    } else {
        Serial.println("OLED SUCCESS");
    }
    u8g2.setFont(u8g2_font_6x10_tr);
    u8g2.clearDisplay();
    u8g2.setDisplayRotation(U8G2_R0);
    u8g2.setFlipMode(0);
    draw();
}

void loop()
{
    // Make sure to call update as fast as possible
    pox.update();
    mesh.update();
    //if (goToSleep == 6){
      //  enterDeepsleep();  funzt noch nicht
    //}

}

